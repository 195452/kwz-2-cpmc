#include <iostream>
#include <vector>
#include <fstream>
#include <list>
#include <algorithm>
#include <set>
#include <string>
#include <limits>

#define SIZETMAX numeric_limits<size_t>::max()

using namespace std;

class Zadanie
{
public:
    Zadanie(size_t czas_, size_t min_czas_, size_t koszt_skrocenia_, size_t id);
    Zadanie();
    size_t id,czas,es,ef,ls,lf,koszt_skrocenia,min_czas;
    
    bool operator< (const Zadanie & zad2) const
    {
        return koszt_skrocenia < zad2.koszt_skrocenia;
    }
};

Zadanie::Zadanie(size_t czas_, size_t min_czas_, size_t koszt_skrocenia_, size_t id_)
{
    czas=czas_;
    id=id_;
    koszt_skrocenia=koszt_skrocenia_;
    min_czas=min_czas_;
}


class Projekt
{
    vector<Zadanie> zadania; // zadania w kolejnosci zgodnej z id

    vector<size_t> kolejnosc_topologiczna; // kolejnosc topologiczna (w formie id zadan)
    vector<size_t> kolejnosc_topologiczna_odwrocona; // kolejnosc topologiczna dla odwroconych kierukow

    vector<size_t> zadania_posortowane; // posortowane po koszcie skrocenia (w formie id zadan)

    vector< set<size_t> > krawedzie_do; //krawedzie wychodzace z kazdego wierzcholka
    vector< set<size_t> > krawedzie_z; //krawedze wchodzace do kazdego wierzcholka

    size_t dlugosc;
    size_t budzet;
    size_t koszt_redukcji;
    vector<size_t> redukcja; //o ile zredukowano kazde zadanie (po [id])

    vector<size_t> sciezka_krytyczna;

public:
    int wczytaj(string nazwa_pliku);

    void sortuj_topologicznie(); // wynik w zmiennej kolejnosc_topologiczna
    size_t es_ef_dlugosc_projektu_topologicznie();
    void ls_lf(); //przejscie wstecz i oblicznie es oraz lf dla kazdego zadania

    void odwiedz(size_t nr_wierzcholka, size_t last_finish, vector<size_t> sciezka); //rekurencyjna do znajdywania sciezki krytycznej
    void znajdz_sciezke_krytyczna();

    void wyswietl_zadania();
    void wyswietl_sciezke_krytyczna();

    void redukuj();
    void redukuj_gorzej();

    void wyswietl_redukcje();

    size_t get_dlugosc() { return dlugosc; };
    size_t get_budzet() { return budzet; };
    size_t get_koszt_redukcji() { return koszt_redukcji; };

    Projekt()
    {
        koszt_redukcji=0;
    }

};

void Projekt::sortuj_topologicznie()
{
    list<Zadanie> zadania_lista(zadania.begin(),zadania.end()); //lista zeby szybciej usuwac elementy

    list<Zadanie>::iterator zadania_itr=zadania_lista.begin();

    vector< set<size_t> > krawedzie_z_tmp = krawedzie_z; //kopia, bo bedziemy usuwac krawedzie
    vector< set<size_t> > krawedzie_do_tmp = krawedzie_do;

    while(zadania_lista.size()>1)
    {
        while(!(krawedzie_do_tmp[(*zadania_itr).id].empty()) && zadania_itr!=(--zadania_lista.end())) //szukanie wierzcholka o stopniu wchodzacym 0..
            zadania_itr++;
        kolejnosc_topologiczna.push_back((*zadania_itr).id); //...dodawanie go do kolejnosci
        //usuwanie krawedzi zwiazanych z tym wierzcholkiem oraz jego samego
        for (auto &itr: krawedzie_z_tmp[(*zadania_itr).id])
        {
            krawedzie_do_tmp[itr].erase((*zadania_itr).id);
        }
        for (auto &itr: krawedzie_do_tmp[(*zadania_itr).id])
        {
            krawedzie_z_tmp[itr].erase((*zadania_itr).id);
        }
        krawedzie_z_tmp[(*zadania_itr).id].clear();
        krawedzie_do_tmp[(*zadania_itr).id].clear();

        zadania_lista.erase(zadania_itr);
        zadania_itr=zadania_lista.begin();
    }
    kolejnosc_topologiczna.push_back(zadania_lista.front().id); //ostatnie zadanie
    //zapisanie odwrotnej kolejnosci
    kolejnosc_topologiczna_odwrocona.resize(kolejnosc_topologiczna.size());
    reverse_copy(kolejnosc_topologiczna.begin(),kolejnosc_topologiczna.end(),kolejnosc_topologiczna_odwrocona.begin());
}


size_t Projekt::es_ef_dlugosc_projektu_topologicznie()
{
    size_t max_ef=0;
    for(auto &itr: kolejnosc_topologiczna)
    {
        size_t tmp_ef=0;
        for(auto &itr2: krawedzie_do[itr])
        {
            tmp_ef=max(tmp_ef, zadania[itr2].ef);
        }
        zadania[itr].es=tmp_ef;
        zadania[itr].ef=tmp_ef+zadania[itr].czas;
        max_ef=max(max_ef,zadania[itr].ef);
    }
    dlugosc=max_ef;
    return max_ef;
}

void Projekt::ls_lf()
{
    zadania[kolejnosc_topologiczna_odwrocona[0]].lf=dlugosc;
    zadania[kolejnosc_topologiczna_odwrocona[0]].ls=zadania[kolejnosc_topologiczna_odwrocona[0]].lf-zadania[kolejnosc_topologiczna_odwrocona[0]].czas;
    for(auto &itr: kolejnosc_topologiczna_odwrocona)
    {
        size_t tmp_ls=dlugosc;
        for(auto &itr2: krawedzie_z[itr])
        {
            tmp_ls=min(tmp_ls, zadania[itr2].ls);
        }
        zadania[itr].lf=tmp_ls;
        zadania[itr].ls=tmp_ls-zadania[itr].czas;
    }
}

//nr_wierzcholka - odwiedzany wierzcholek
//last finish - koniec poprzedniego zadania (wierzcholka)
//sciazka - zapamietuje przechodzona sciezke
void Projekt::odwiedz(size_t nr_wierzcholka, size_t last_finish, vector<size_t> sciezka)
{
    if (zadania[nr_wierzcholka].lf==zadania[nr_wierzcholka].ef && last_finish==zadania[nr_wierzcholka].es) //zadanie bez poslizgu
    {
        sciezka.push_back(nr_wierzcholka);
        if (zadania[nr_wierzcholka].ef==dlugosc) //jezeli jest koncem
        {
            sciezka_krytyczna=sciezka;
        }
        else
        {
            for(auto & itr: krawedzie_z[nr_wierzcholka])
            {
                odwiedz(itr, zadania[nr_wierzcholka].ef, sciezka);
            }
        }
    }
}

void Projekt::znajdz_sciezke_krytyczna()
{
    for (auto & itr: kolejnosc_topologiczna)
    {
        if (krawedzie_do[itr].empty())
            odwiedz(itr,0,vector<size_t>());
    }
}

void Projekt::wyswietl_zadania()
{
    for(auto &itr: zadania)
    {
        cout << itr.es << " " << itr.ef << " " << itr.ls << " " << itr.lf << endl;
    }
}

void Projekt::wyswietl_sciezke_krytyczna()
{
    for(auto & itr: sciezka_krytyczna)
        cout << itr+1 << " " << zadania[itr].es << " " << zadania[itr].ef << endl;
}

void Projekt::wyswietl_redukcje()
{
    for(auto & itr: redukcja)
        cout << itr << " ";
}


int Projekt::wczytaj(string nazwa_pliku)
{
    ifstream plik;
    size_t i;
    size_t liczba_zadan=0;
    size_t liczba_polaczen=0;

    plik.open(nazwa_pliku.c_str(),ios::out);
    if (!(plik.is_open()))
    {
        cout << "Blad otwierania pliku";
        return 0;
    }
    else
    {
        plik >> liczba_zadan;
        plik >> liczba_polaczen;
        plik >> budzet;
        redukcja.resize(liczba_zadan);
        for(i=0;i<liczba_zadan;i++)
        {
            size_t czas_tmp,koszt_tmp,min_tmp;
            plik >> czas_tmp;
            plik >> min_tmp;
            plik >> koszt_tmp;
            redukcja[i]=0;
            zadania.push_back(Zadanie(czas_tmp,min_tmp,koszt_tmp,i));
            krawedzie_do.push_back(set<size_t>());
            krawedzie_z.push_back(set<size_t>());

        }
        for(i=0;i<liczba_polaczen;i++)
        {
            size_t krawedz_z, krawedz_do;
            plik >> krawedz_z;
            plik >> krawedz_do;
            krawedzie_do[krawedz_do-1].insert(krawedz_z-1);
            krawedzie_z[krawedz_z-1].insert(krawedz_do-1);
        }
    }
    plik.close();
    return 1;
}

void Projekt::redukuj()
{
    sortuj_topologicznie();
    ls_lf();
    znajdz_sciezke_krytyczna();
    size_t zadanie_do_skrocenia=0;
    while(zadanie_do_skrocenia!=SIZETMAX) {

        size_t koszt_min_tmp=SIZETMAX;
        zadanie_do_skrocenia=SIZETMAX;
        for(size_t j=0; j<sciezka_krytyczna.size();j++)
        {
            if (zadania[sciezka_krytyczna[j]].koszt_skrocenia < koszt_min_tmp
                    && zadania[sciezka_krytyczna[j]].czas > zadania[sciezka_krytyczna[j]].min_czas
                    && zadania[sciezka_krytyczna[j]].koszt_skrocenia<=budzet) {

                koszt_min_tmp = zadania[sciezka_krytyczna[j]].koszt_skrocenia;
                zadanie_do_skrocenia = sciezka_krytyczna[j];
            }
        }
        if (zadanie_do_skrocenia!=SIZETMAX)
        {
            budzet-=zadania[zadanie_do_skrocenia].koszt_skrocenia;
            koszt_redukcji+=zadania[zadanie_do_skrocenia].koszt_skrocenia;
            zadania[zadanie_do_skrocenia].czas--;
            redukcja[zadanie_do_skrocenia]++;
            es_ef_dlugosc_projektu_topologicznie();
            ls_lf();
            znajdz_sciezke_krytyczna();
        }
    }
}


void Projekt::redukuj_gorzej()
{
    auto zadania_sort=zadania;
    sort(zadania_sort.rbegin(),zadania_sort.rend()); //posortowanie odwrotnie (od najmniejszego kosztu)
    zadania_posortowane.reserve(zadania_sort.size());
    for(auto & zadanie: zadania_sort)   //zapisanie kolejnosci sortowania
        zadania_posortowane.push_back(zadanie.id);
    zadania_sort.clear();   //czyszczenie

    for(auto & zadanie: zadania_posortowane) //przejscie po id posortowanych zadan
    {
        while(zadania[zadanie].koszt_skrocenia<=budzet && zadania[zadanie].czas>zadania[zadanie].min_czas)
        {
            zadania[zadanie].czas--;
            budzet-=zadania[zadanie].koszt_skrocenia;
            koszt_redukcji+=zadania[zadanie].koszt_skrocenia;
            redukcja[zadanie]++;
        }
    }

}

int main()
{
    Projekt projekt;
    Projekt projekt2;

    string nazwa_pliku = "data40.txt";

    if (projekt.wczytaj(nazwa_pliku.c_str()))
    {
        projekt.sortuj_topologicznie();
        cout << "no reduction scheduler time:" << endl << projekt.es_ef_dlugosc_projektu_topologicznie() << endl;
        projekt.redukuj();
        cout << "reduction scheduler time:" << endl << projekt.get_dlugosc() << endl <<  "reduction cost:" << endl <<  projekt.get_koszt_redukcji() << endl;
        cout << "reduction:" << endl;
        projekt2.wyswietl_redukcje();
        cout << endl;

    }
    
    if (projekt2.wczytaj(nazwa_pliku.c_str()))
    {
        projekt2.sortuj_topologicznie();
        cout << "no reduction scheduler time:" << endl << projekt2.es_ef_dlugosc_projektu_topologicznie() << endl;
        projekt2.redukuj_gorzej();
        projekt2.sortuj_topologicznie();
        cout << "reduction scheduler time:" << endl << projekt2.es_ef_dlugosc_projektu_topologicznie() << endl <<  "reduction cost:" << endl <<  projekt2.get_koszt_redukcji() << endl;
        cout << "reduction:" << endl;
        projekt2.wyswietl_redukcje();
        
    }
    return 0;
}

